# Python Package Template

![Build status](https://git.astron.nl/templates/python-package/badges/main/pipeline.svg)
![Test coverage](https://git.astron.nl/templates/python-package/badges/main/coverage.svg)

Template to create Python repositories with CI/CD pipeline for building, testing and publishing a python package.

If you find some missing functionality with regards to CI/CD, testing, linting or something else, feel free to make a merge request with the proposed changes.

## How to apply this template

This templates uses `cookiecutter` which can be

```bash
pip install --user cookiecutter
```

Then you can create an instance of your template with:

```bash
cookiecutter https://git.astron.nl/templates/python-package.git
# Next follow a set of prompts (such as the name and description of the package)
```

## Gitlab security, secrets and role configuration

When using these templates for a repository on git.astron.nl please read the following
pages to configure Gitlab appropriately:

1. [Gitlab Repository Configuration](https://git.astron.nl/groups/templates/-/wikis/Gitlab-Repository-Configuration)
2. [Continuous delivery guideline](https://git.astron.nl/groups/templates/-/wikis/Continuous%20Delivery%20Guideline)

## Setup

Once you have used the template there are some additional steps to fully use this
repository on Gitlab.

1. [Cleanup Docker Registry Images](https://git.astron.nl/groups/templates/-/wikis/Cleanup-Docker-Registry-Images)
2. [Setup Protected Verson Tags](https://git.astron.nl/groups/templates/-/wikis/Setting-up-Protected-Version-Tags)

## License
This project is licensed under the Apache License Version 2.0
